# My story with testing

Learning from Anthony Fox

Going through the "obey the goat" book

New job - dealing with data model that has many parent of parent etc

wrote a script - I did some TDD

client came back with two separate changes

TDD saved the day because I didn't have to do all the set up and check for the different cases to check for

so the ability to make code changes 


I have for a long time thought "testing sounds nice but `data is different`".  Recently I have pushed myself to be more TDD oriented.  I experienced one of the major benefits of having tests.  
- changing requirements on an existing load of business rules for one of my tasks
- could add a test for that new requirement and feel very comfortable that my existing tests would act as bumpers as I changed code.  I could freely make a change for the new requirement with the knowledge that breaking existing rules would be expressed in the existing tests.

I also built a class that would be subclassed in a sqlalchemy model definition.  This class creates records in the parent tables of the desired table to test against.  This allowed me to write the tests without having to recreate the data 

for example neutral override
then came change that if no matching ccn existed to exclude them out
then came no duplicates - had to test that combination of tin npi and ccn would create one record whereas different tin but same npi and different ccn creates two records